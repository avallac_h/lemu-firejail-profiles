#!/bin/sh
set -e

SCRIPTDIR="$(dirname "$0")"

find "${SCRIPTDIR}/profiles" -maxdepth 2 -type f -name "*.profile"  \
    -printf '%f\n'                                                  \
    | sed -e 's|.profile||'                                         \
    | sort                                                          \
    > "${SCRIPTDIR}/profiles/firecfg.config.local"

echo "All done." >&2
