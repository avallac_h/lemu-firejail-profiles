#!/bin/sh
set -e

SCRIPTDIR="$(dirname "$0")"
test -z "$DESTDIR" && DESTDIR="/etc/firejail/"

install -vdm0755 "$DESTDIR"

find "$SCRIPTDIR/profiles" -type f                              \
    \( -name "*.profile" -o -name "*.local" -o -name "*.inc" \) \
    -exec install -vm0644 -t "$DESTDIR" '{}' \;

echo "All done." >&2
