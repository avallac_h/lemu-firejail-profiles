#!/bin/sh
set -e

SCRIPTDIR="$(dirname "$0")"

for HOOK in "${SCRIPTDIR}/hooks/"* ; do
    find "$SCRIPTDIR" -type f \
        \( -name "*.profile" -o -name "*.local" \) \
        -exec "$HOOK" '{}' \;
done

echo "All done." >&2
