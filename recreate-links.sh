#!/bin/sh
set -e

SCRIPTDIR="$(dirname "$0")"
DESTDIR="${SCRIPTDIR}/profiles.links"

echo '==> Cleaning...' >&2

find "$DESTDIR" -type l                                         \
    \( -name "*.profile" -o -name "*.local" -o -name "*.inc" \) \
    -print -delete

echo '==> Re-creating links...' >&2

# Short-term madness resulting interesting side-effect with '../././'
#find "$SCRIPTDIR" -maxdepth 2 -type f           \
#    \( -name "*.profile" -o -name "*.local" \)  \
#    -print0                                     \
#    | xargs -0 -n1 -I {}                        \
#    sh -c "ln -sv -t \"$DESTDIR\" \"../$(basename \"$(dirname \"{}\")\")/$(basename \"{}\")\""

find "${SCRIPTDIR}/profiles" -maxdepth 2 -type f                \
    \( -name "*.profile" -o -name "*.local" -o -name "*.inc" \) \
    -print                                                      \
    | while IFS= read -r TARGET ; do                            \
        DIRNAME=$(basename "$(dirname "$TARGET")") ;            \
        BASENAME=$(basename "$TARGET") ;                        \
        ln -sv -t "$DESTDIR" "../profiles/${DIRNAME}/${BASENAME}" ;  \
    done
rm "${DESTDIR}/firecfg.config.local"

echo "All done." >&2
