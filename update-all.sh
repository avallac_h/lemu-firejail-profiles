#!/bin/sh
set -e

SCRIPTDIR="$(dirname "$0")"

"${SCRIPTDIR}/recreate-firecfg-local-config.sh"
"${SCRIPTDIR}/recreate-links.sh"
"${SCRIPTDIR}/run-hooks.sh"

echo "All done. I mean really all." >&2
