#!/bin/sh
set -e

_me=$(basename "$0")

if [ -h "$1" ] ; then
    filename=$(readlink "$1")
else
    filename="$1"
fi

if grep '^# vim: set' "$filename" >/dev/null ; then
    echo "$_me: $1: Modeline already exists." >&2
    exit 0
fi

mtime=$(stat -c "%y" "$filename")
echo "$_me: $1: Preserved mtime: $mtime" >&2

printf '\n# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:' >> "$filename"

touch -d "$mtime" "$filename"

