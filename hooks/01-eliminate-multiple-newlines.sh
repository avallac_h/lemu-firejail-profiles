#!/bin/sh
set -e

_me=$(basename "$0")

if [ -h "$1" ] ; then
    filename=$(readlink "$1")
else
    filename="$1"
fi
mtime=$(stat -c "%y" "$filename")
echo "$_me: $1: Preserved mtime: $mtime" >&2

# <https://stackoverflow.com/questions/52461314/can-not-replace-multiple-empty-lines-with-one>
perl -00 -lpe 1 -i "$filename"

touch -d "$mtime" "$filename"

