# Firejail profile for retroarch
# Description: Multi-system console emulator
# This file is custom profile based on wesnoth profile
# Persistent local customizations
include retroarch.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/retroarch
noblacklist ${HOME}/Emulation

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-passwdmgr.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.config/retroarch
mkdir ${HOME}/Emulation
whitelist ${HOME}/.config/retroarch
whitelist ${HOME}/Emulation

include whitelist-common.inc
include whitelist-var-common.inc

caps.drop all
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix,netlink
seccomp
shell none
tracelog

disable-mnt
private-cache
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

