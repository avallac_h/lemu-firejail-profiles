# Firejaila custom profile for PPSSPPSDL
# This file is NOT overwritten after every install/update
# Persistent local customizations
include PPSSPPSDL.local
# added by included profile
#include globals.local

# Redirect
include ppsspp.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

