# Firejail profile for pcsxr
# Description: Sony PlayStation emulator
# This file is custom profile based on wesnoth profile
# Persistent local customizations
include pcsxr.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.pcsxr
noblacklist ${HOME}/Emulation

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.pcsxr
mkdir ${HOME}/Emulation
whitelist ${HOME}/.pcsxr
whitelist ${HOME}/Emulation

include whitelist-common.inc
include whitelist-var-common.inc

caps.drop all
ipc-namespace
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix,netlink
seccomp
shell none
tracelog

disable-mnt
private-cache
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

