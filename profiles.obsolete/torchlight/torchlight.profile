# Firejail profile for Torchlight
# Description: A hack and slash action role-playing game
# This file is overwritten after every install/update
# Persistent local customizations
include torchlight.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.runicgames

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-passwdmgr.inc
include disable-programs.inc
include disable-shell.inc
include disable-xdg.inc

mkdir ${HOME}/.runicgames
whitelist ${HOME}/.runicgames

include whitelist-common.inc

apparmor
caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

disable-mnt
private-bin torchlight
private-cache
private-dev
#private-etc alternatives,asound.conf,ca-certificates,crypto-policies,drirc,fonts,group,host.conf,hostname,hosts,ld.so.cache,ld.so.preload,localtime,machine-id,nsswitch.conf,passwd,pki,pulse,resolv.conf,ss
private-opt Torchlight
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

