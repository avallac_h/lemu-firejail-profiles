# Firejail profile for yt-dlp
# Description: Downloader of videos from YouTube and other sites
# This file is NOT overwritten after every install/update
quiet
# Persistent local customizations
include yl-dlp.local
# Persistent global definitions
# added by included profile
#include globals.local

noblacklist ${HOME}/.cache/yt-dlp
noblacklist ${HOME}/.config/yt-dlp

private-bin env,ffmpeg,python*,yt-dlp

# Redirect
include youtube-dl.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

