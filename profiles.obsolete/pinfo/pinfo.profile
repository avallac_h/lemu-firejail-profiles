# Firejail custom profile for pinfo based on less profile
# Description: Curses based lynx-style info browser
# This file is NOT overwritten after every install/update
quiet
# Persistent local customizations
include pinfo.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.pinforc

include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-passwdmgr.inc

apparmor
caps.drop all
ipc-namespace
machine-id
net none
no3d
nodbus
nodvd
nonewprivs
#noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog
x11 none

# The user can have a custom commands configured in ${HOME}/.pinforc.
# Enable private-bin and private-lib if you are not using any filter.
# private-bin pinfo,apropos,man,links,lynx
# private-lib
private-cache
private-dev
writable-var-log

memory-deny-write-execute
read-only ${HOME}
read-write ${HOME}/.pinforc

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

