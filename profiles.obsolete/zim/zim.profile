# Firejail profile for zim
# Description: A desktop wiki application
# ** This custom profile based on inkscape profile **
# Persistent local customizations
include zim.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.cache/zim
noblacklist ${HOME}/.config/zim
noblacklist ${HOME}/Notebooks

# required by vcs plugin
read-only ${HOME}/.config/git
read-only ${HOME}/.gitconfig
read-only ${HOME}/.git-credentials
read-only ${HOME}/.hgrc

noblacklist ${DESKTOP}
noblacklist ${DOCUMENTS}
read-only ${PICTURES}

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-passwdmgr.inc
include disable-programs.inc
include disable-xdg.inc

whitelist /usr/share/zim
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
machine-id
net none
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

private-bin zim,python*,env,import,scrot,bzr,git,fossil,hg
private-dev
private-tmp

dbus-user none
dbus-system none

# memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

