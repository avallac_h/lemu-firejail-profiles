# Firejail profile for mcomix
# Description: Image viewer specifically designed to handle comic books
# This file is overwritten after every install/update
# Persistent local customizations
include mcomix.local
# Persistent global definitions
include globals.local

noblacklist /usr/share/mcomix
noblacklist ${HOME}/.config/mcomix
noblacklist ${HOME}/.local/share/mcomix

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc

mkdir ${HOME}/.config/mcomix
mkdir ${HOME}/.local/share/data/mcomix

whitelist /usr/share/mcomix
whitelist ${DOWNLOADS}
whitelist ${HOME}/.config/mcomix
whitelist ${HOME}/.local/share/mcomix

include whitelist-common.inc
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
machine-id
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

private-bin python*,mcomix,comicthumb
private-cache
private-dev
private-tmp

#memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

