# Firejail profile for higan
# Description: Multi-system console emulator
# This file is custom profile based on wesnoth profile
# Persistent local customizations
include higan.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/higan
noblacklist ${HOME}/.local/share/higan
noblacklist ${HOME}/Emulation

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.config/higan
mkdir ${HOME}/.local/share/higan
mkdir ${HOME}/Emulation

whitelist ${HOME}/usr/share/higan
whitelist ${HOME}/usr/share/icarus
whitelist ${HOME}/.config/higan
whitelist ${HOME}/.local/share/higan
whitelist ${HOME}/Emulation

include whitelist-common.inc
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

caps.drop all
net none
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
tracelog

disable-mnt
#private-bin higan,sh
private-cache
#private-dev
#private-etc pulse,X11,fonts,machine-id,asound.conf
private-tmp

noexec ${HOME}
noexec /tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

