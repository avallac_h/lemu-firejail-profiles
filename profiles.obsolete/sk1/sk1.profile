# Firejail [custom] [whitelisted] profile for sk1 based on inkscape profile
# Description: Vector-based drawing program
# This file IS NOT overwritten after every install/update
# Whitelist reason: russian authors

# Persistent local customizations
include sk1.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/sk1-wx

noblacklist ${DESKTOP}
noblacklist ${DOCUMENTS}
noblacklist ${PICTURES}

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-passwdmgr.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.config/sk1-wx
whitelist ${HOME}/.config/sk1-wx

include whitelist-common.inc
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
machine-id
net none
no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

disable-mnt
private-bin sk1,python*
private-cache
private-etc alternatives,dconf,fonts,gconf,gtk-2.0,gtk-3.0,locale,locale.alias,locale.conf,mime.types,xdg
private-dev
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

