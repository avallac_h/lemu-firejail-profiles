# Firejail profile for goldendict
# Description: 	Feature-rich dictionary lookup program
# This file is overwritten after every install/update
# Persistent local customizations
include goldendict.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.goldendict
noblacklist ${HOME}/.lingvo
noblacklist ${DESKTOP}

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-passwdmgr.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.goldendict
whitelist ${HOME}/.goldendict
whitelist ${HOME}/.lingvo

include whitelist-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix,netlink
seccomp
shell none

# private-bin goldendict
private-dev
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

