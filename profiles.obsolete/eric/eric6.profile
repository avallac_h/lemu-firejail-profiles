# Firejail profile for Eric Python IDE
# Persistent local customizations
include eric6.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/Eric6
noblacklist ${HOME}/.eric6
noblacklist ${HOME}/.local/share/Eric6

# Allows files commonly used by IDEs
include allow-common-devel.inc

include disable-common.inc
include disable-passwdmgr.inc
include disable-programs.inc

include whitelist-common.inc
include whitelist-var-common.inc

mkdir ${HOME}/.config/Eric6
mkdir ${HOME}/.eric6
mkdir ${HOME}/.local/share/Eric6

whitelist ${HOME}/.config/Eric6
whitelist ${HOME}/.eric6
whitelist ${HOME}/.local/share/Eric6

caps.drop all
netfilter
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix,inet,inet6,netlink
#seccomp
seccomp !chroot
shell none

private-cache
private-dev
private-tmp

# Disabling noexec ${HOME} for now since it will
# probably interfere with running some programmes
# in Eric IDE
# noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

