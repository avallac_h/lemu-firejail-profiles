# Firejail profile for mkvmerge
# Description: 
# This file is custom profile based on mp3splt profile
# Persistent local customizations
include mkvmerge.local
# Persistent global definitions
include globals.local

noblacklist ${DOWNLOADS}
noblacklist ${MUSIC}
noblacklist ${VIDEOS}

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
machine-id
net none
no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

disable-mnt
private-bin mkvextract,mkvinfo,mkvmerge,mkvpropedit
private-cache
private-dev
private-etc alternatives
private-tmp

memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

