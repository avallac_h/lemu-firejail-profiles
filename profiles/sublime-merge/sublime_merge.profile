# Firejail profile for sublime_merge
# Description: Sublime Merge Git client
# Persistent local customizations
include sublime_merge.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/sublime-merge
noblacklist ${HOME}/.gitconfig
#noblacklist ${HOME}/.git-credentials
#noblacklist ${HOME}/.ssh

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-programs.inc
include disable-interpreters.inc

mkdir ${HOME}/.config/sublime-merge

whitelist ${HOME}/.config/sublime-merge
whitelist /opt/sublime_merge
whitelist ${HOME}/.gitconfig

include whitelist-common.inc
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

caps.drop all
#netfilter
net none
no3d
nogroups
nonewprivs
noroot
notv
novideo
protocol unix
#protocol unix,inet,inet6
seccomp
shell none
tracelog

private-bin sublime_merge,git,ssh
private-cache
private-dev
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

