#!/usr/bin/env python

import re
import sys

from pathlib import Path
from pprint import pprint as PP


def get_path(line: str) -> str:
    line = line.rstrip()

    match = re.match(
        r"^\s*(?:whitelist|read-only)+\s+(\${HOME}/.*?)$", line, re.DOTALL
    )
    if match:
        return Path(match.group(1))
    else:
        return ""


def main():
    files = [
        Path("/etc/firejail/whitelist-common.inc"),
        Path("/etc/firejail/whitelist-common.local"),
    ]

    pathlist = []
    for filename in files:
        with filename.open("r") as fp:
            for line in fp.readlines():
                path = get_path(line)
                if path:
                    pathlist.append(path)

    filtered = []
    for p in pathlist:
        if not p.is_relative_to("${HOME}/.config"):
            filtered.append("/".join(str(p).split("/")[0:2]))
        else:
            filtered.append(str(p))

    filtered = sorted(list(set(filtered)))

    with open("whitelist-common-noexec.inc", "w") as fp:
        fp.write("# This file SHOULD NOT BE OVERWRITTEN during software install.\n")
        fp.write("# Persistent customizations should go in a .local file.\n")
        fp.write("include whitelist-common-noexec.local\n\n")

        for e in filtered:
            fp.write("noexec {}\n".format(e))


if __name__ == "__main__":
    main()
