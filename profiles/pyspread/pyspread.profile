# Firejail custom profile for pyspread based on picard profile
# Description: Python based non-traditional spreadsheet application
# This file is NOT overwritten after every install/update
# Persistent local customizations
include pyspread.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/pyspread
noblacklist ${DESKTOP}
noblacklist ${DOCUMENTS}

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

include whitelist-var-common.inc

caps.drop all
net none
no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none

disable-mnt
private-bin bash,python*,pyspread
private-cache
private-dev
private-tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

