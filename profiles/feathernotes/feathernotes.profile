# Firejail profile for feathernotes
# Description: Lightweight Qt hierarchical notes-manager
# Based on: leafpad.profile
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include feathernotes.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/feathernotes

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc

include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
net none
#no3d
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
seccomp.block-secondary
shell none
tracelog

private-bin feathernotes
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

