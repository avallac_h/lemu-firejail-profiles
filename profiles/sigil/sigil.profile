# Firejail profile for sigil
# Description: EPUB ebook Editor
# ** This custom profile based on gimp profile **
# Persistent local customizations
include sigil.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.cache/sigil
noblacklist ${HOME}/.config/sigil
noblacklist ${HOME}/.QtWebEngineProcess
noblacklist ${DOCUMENTS}
noblacklist ${PICTURES}

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-devel.inc
include disable-programs.inc
include disable-xdg.inc

whitelist /usr/share/sigil
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
net none
# no3d
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
protocol unix
seccomp !chroot
shell none
#tracelog

#private-bin sigil,pageedit,python*
private-dev
#private-tmp

dbus-user none
dbus-system none

#memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

