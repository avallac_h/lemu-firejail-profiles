# Firejail custom profile for kcmviewer based on zeal profile
# Description: Qt CHM and EPUB reader
# This file is NOT overwritten after every install/update
# Persistent local customizations
include kchmviewer.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/Ulduzsoft
noblacklist ${HOME}/.kchmviewer
noblacklist ${DOCUMENTS}

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc
include disable-xdg.inc

mkdir ${HOME}/.config/Ulduzsoft
mkdir ${HOME}/.kchmviewer

whitelist ${HOME}/.config/Ulduzsoft
whitelist ${HOME}/.kchmviewer
whitelist ${DOCUMENTS}

include whitelist-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
machine-id
net none
no3d
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

disable-mnt
private-bin kchmviewer
private-cache
private-dev
private-etc alternatives,ca-certificates,crypto-policies,fonts,host.conf,hostname,hosts,ld.so.cache,ld.so.conf,ld.so.conf.d,ld.so.preload,locale,locale.alias,locale.conf,localtime,mime.types,nsswitch.conf,pango,pki,protocols,resolv.conf,rpc,services,ssl,Trolltech.conf,X11,xdg
private-tmp

dbus-user none
dbus-system none

# memory-deny-write-execute - breaks on Arch

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

