# Firejail profile for openscad
# Description: Extensible Open Source CAx program
# This file is overwritten after every install/update
# Persistent local customizations
include openscad.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/OpenSCAD
noblacklist ${HOME}/.local/share/OpenSCAD
noblacklist ${DOCUMENTS}

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
#nosound - required by Qt
notv
nou2f
novideo
protocol unix
seccomp
shell none

private-bin openscad,python*
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

