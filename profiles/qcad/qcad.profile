# Firejail profile for qcad
# Description: Extensible Open Source CAx program
# This file is overwritten after every install/update
# Persistent local customizations
include qcad.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/QCAD
noblacklist ${DOCUMENTS}

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
#nosound - required by Qt
notv
nou2f
novideo
protocol unix
seccomp
shell none

private-bin qcad,sh
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

