# Firejail profile for scummvm
# Description: A virtual machine for classic graphical point-and-click adventure games
# Persistent local customizations
include scummvm.local
# Persistent global definitions
include globals.local

# Note: you must whitelist your games folder in your scummvm.local.

noblacklist ${HOME}/.config/scummvm
noblacklist ${HOME}/.local/share/scummvm
noblacklist /usr/share/scummvm

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc
include disable-write-mnt.inc
include disable-xdg.inc

mkdir ${HOME}/.config/scummvm
mkdir ${HOME}/.local/share/scummvm
whitelist ${HOME}/.config/scummvm
whitelist ${HOME}/.local/share/scummvm
include whitelist-common.inc
#include whitelist-runuser-common.inc
#include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix,netlink
seccomp
shell none
tracelog

private-bin scummvm
private-cache
private-dev
private-etc alsa,alternatives,asound.conf,bumblebee,ca-certificates,crypto-policies,dconf,drirc,fonts,gconf,glvnd,gtk-2.0,gtk-3.0,host.conf,hostname,hosts,ld.so.cache,ld.so.conf,ld.so.conf.d,ld.so.preload,locale,locale.alias,locale.conf,localtime,machine-id,mime.types,nsswitch.conf,nvidia,pango,pki,protocols,pulse,resolv.conf,rpc,services,ssl,X11,xdg
private-opt none
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

