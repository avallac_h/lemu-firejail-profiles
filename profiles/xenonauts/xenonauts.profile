# Firejail profile alias for Xenonauts
# This file is overwritten after every install/update

# Persistent local customizations
include xenonauts.local

# Redirect
include Xenonauts.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

