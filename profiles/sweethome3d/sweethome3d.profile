# Firejail profile for sweethome3d
# Description: Turn-based multi-player strategy game
# This file is overwritten after every install/update
# Persistent local customizations
include sweethome3d.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.eteks/sweethome3d

# Allow java (blacklisted by disable-devel.inc)
include allow-java.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.java
mkdir ${HOME}/.eteks/sweethome3d

whitelist ${HOME}/.java
whitelist ${HOME}/.eteks/sweethome3d

include whitelist-common.inc
#include whitelist-usr-share-common.inc
include whitelist-var-common.inc

caps.drop all
ipc-namespace
net none
nodvd
nogroups
noinput
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

disable-mnt
private-cache
#private-dev
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

