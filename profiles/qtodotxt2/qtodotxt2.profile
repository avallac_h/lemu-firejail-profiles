# Firejail custom profile for qtodotxt2 based on picard profile
# Description: A cross platform todo.txt GUI using QML
# This file is NOT overwritten after every install/update
# Persistent local customizations
include qtodotxt2.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/QTodoTxt
noblacklist ${DESKTOP}
noblacklist ${DOCUMENTS}

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

include whitelist-var-common.inc

caps.drop all
net none
no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none

disable-mnt
private-bin python*,bash,qtodotxt2
private-cache
private-dev
private-tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

