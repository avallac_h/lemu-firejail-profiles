# Firejail profile for asciidoctor
# Description: AsciiDoc markup converter implemented in Ruby
# Based on: pandoc.profile
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include asciidoctor.local
# Persistent global definitions
include globals.local

# Allow ruby (blacklisted by disable-interpreters.inc)
include allow-ruby.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc
include disable-X11.inc
include disable-xdg.inc

# FIXME: apparmor: denied file_lock (w) on /usr/lib/ruby/gems/3.0.0/specifications/default/abbrev-0.1.0.gemspec
#apparmor
caps.drop all
ipc-namespace
machine-id
net none
no3d
nodvd
nogroups
noinput
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
seccomp.block-secondary
shell none
tracelog
x11 none

disable-mnt
private-bin asciidoctor,ruby,ruby-*.*
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

ignore memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

