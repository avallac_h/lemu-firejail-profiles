# Firejail profile for naev
# Description: Multi-system console emulator
# This file is custom profile based on wesnoth profile
# Persistent local customizations
include naev.local
# Persistent global definitions
include globals.local

# Allow lua
include allow-lua.inc

noblacklist ${HOME}/.config/naev
noblacklist ${HOME}/.local/share/naev

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-passwdmgr.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.config/naev
mkdir ${HOME}/.local/share/naev

whitelist ${HOME}/.config/naev
whitelist ${HOME}/.local/share/naev

include whitelist-common.inc

caps.drop all
net none
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

disable-mnt
private-bin naev
private-cache
private-dev
private-etc pulse,X11,fonts,machine-id,asound.conf
private-tmp

noexec ${HOME}
noexec /tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

