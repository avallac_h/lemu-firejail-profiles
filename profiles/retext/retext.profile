# Firejail profile for retext
# Description: Markdown and ReStructuredText editor
# ** This custom profile based on inkscape profile **
# Persistent local customizations
include retext.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.cache/retext
noblacklist ${HOME}/.config/ReText*
noblacklist ${DOCUMENTS}

# Allow python (blacklisted by disable-interpreters.inc)
noblacklist ${PATH}/python2*
noblacklist ${PATH}/python3*
noblacklist /usr/lib/python2*
noblacklist /usr/lib/python3*
noblacklist /usr/local/lib/python2*
noblacklist /usr/local/lib/python3*

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

include whitelist-var-common.inc

apparmor
caps.drop all
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix,netlink
seccomp
shell none

private-bin retext,python*,env
private-dev
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

