# Firejail profile for pelican
# Description: Static site generator powered by Python
# This file is overwritten after every install/update
# Persistent local customizations
include pelican.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/Pelican
noblacklist ${HOME}/public_html

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

# Allow user-wide git-config
include allow-common-devel.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc

mkdir ${HOME}/Pelican
mkdir ${HOME}/public_html

whitelist ${HOME}/Pelican
whitelist ${HOME}/public_html

include whitelist-usr-share-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
machine-id
netfilter
no3d
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix,inet,inet6
seccomp
shell none
tracelog
# x11 none # abstract X11 socket still accessible.

#private-bin pelican*,python*,pandoc
private-etc alternatives,ca-certificates,crypto-policies,fonts,group,host.conf,hostname,hosts,ld.so.cache,ld.so.conf,ld.so.conf.d,ld.so.preload,locale,locale.alias,locale.conf,localtime,login.defs,passwd,pki,resolv.conf,ssl
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

