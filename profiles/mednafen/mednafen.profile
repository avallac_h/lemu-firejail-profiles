# Firejail profile for mednafen
# Description: Multi-system console emulator
# This file is custom profile based on wesnoth profile
# Persistent local customizations
include mednafen.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.mednafen
noblacklist ${HOME}/Emulation

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.mednafen
mkdir ${HOME}/Emulation
whitelist ${HOME}/.mednafen
whitelist ${HOME}/Emulation

include whitelist-common.inc
include whitelist-var-common.inc

caps.drop all
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
tracelog

disable-mnt
#private-bin sh,find,mednafen,icarus
#private-bin mednafen,icarus
private-cache
#private-dev
#private-etc pulse,X11,fonts,machine-id,asound.conf
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

