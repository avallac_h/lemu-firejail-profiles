# Firejail profile for chromium-spotify
# Description: Reversed noexec Spotify Web Player profile
# Based on: whitelist-common.inc + Chromium 98.0.4758.80 config directory
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include chromium-spotify.local
# Persistent global definitions
include globals.local

ignore noexec ${HOME}

noexec ${DOWNLOADS}

include whitelist-common-noexec.inc

# Misc. from chromium-common.profile
noexec ${HOME}/.config/chromium-flags.conf
noexec ${HOME}/.pki

# Chromium dirs
noexec ${HOME}/.config/chromium-spotify/AutofillRegex
noexec ${HOME}/.config/chromium-spotify/AutofillStates
noexec ${HOME}/.config/chromium-spotify/CertificateRevocation
noexec ${HOME}/.config/chromium-spotify/ClientSidePhishing
noexec ${HOME}/.config/chromium-spotify/Crash Reports
noexec ${HOME}/.config/chromium-spotify/Crowd Deny
noexec ${HOME}/.config/chromium-spotify/Default
noexec ${HOME}/.config/chromium-spotify/DesktopSharingHub
noexec ${HOME}/.config/chromium-spotify/Dictionaries
noexec ${HOME}/.config/chromium-spotify/FileTypePolicies
noexec ${HOME}/.config/chromium-spotify/FirstPartySetsPreloaded
noexec ${HOME}/.config/chromium-spotify/Floc
noexec ${HOME}/.config/chromium-spotify/GrShaderCache
noexec ${HOME}/.config/chromium-spotify/MEIPreload
noexec ${HOME}/.config/chromium-spotify/NativeMessagingHosts
noexec ${HOME}/.config/chromium-spotify/OnDeviceHeadSuggestModel
noexec ${HOME}/.config/chromium-spotify/OptimizationHints
noexec ${HOME}/.config/chromium-spotify/OriginTrials
noexec ${HOME}/.config/chromium-spotify/PKIMetadata
noexec ${HOME}/.config/chromium-spotify/SSLErrorAssistant
noexec ${HOME}/.config/chromium-spotify/Safe Browsing
noexec ${HOME}/.config/chromium-spotify/SafetyTips
noexec ${HOME}/.config/chromium-spotify/ShaderCache
noexec ${HOME}/.config/chromium-spotify/Subresource Filter
noexec ${HOME}/.config/chromium-spotify/TLSDeprecationConfig
# enable DRM: noexec ${HOME}/.config/chromium-spotify/WidevineCdm
noexec ${HOME}/.config/chromium-spotify/ZxcvbnData
noexec ${HOME}/.config/chromium-spotify/hyphen-data

# Chromium files
noexec ${HOME}/.config/chromium-spotify/*.pma
noexec ${HOME}/.config/chromium-spotify/First Run
noexec ${HOME}/.config/chromium-spotify/Last Version
noexec ${HOME}/.config/chromium-spotify/Local State
noexec ${HOME}/.config/chromium-spotify/Safe Browsing Cookies
noexec ${HOME}/.config/chromium-spotify/Safe Browsing Cookies-journal
noexec ${HOME}/.config/chromium-spotify/SingletonCookie
noexec ${HOME}/.config/chromium-spotify/SingletonLock
noexec ${HOME}/.config/chromium-spotify/SingletonSocket
noexec ${HOME}/.config/chromium-spotify/Variations

# Data directories
# NOTE: private-cache statement below
noblacklist ${HOME}/.cache/chromium-spotify
noblacklist ${HOME}/.config/chromium-spotify

mkdir ${HOME}/.cache/chromium-spotify
mkdir ${HOME}/.config/chromium-spotify
whitelist ${HOME}/.cache/chromium-spotify
whitelist ${HOME}/.config/chromium-spotify

# Private cache
private-cache

# Redirect
include chromium-common.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

