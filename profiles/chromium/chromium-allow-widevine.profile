# Firejail profile for chromium-allow-widevine
# Description: Reversed noexec Chromium profile to allow Widivine DRM execution
# Based on: whitelist-common.inc + Chromium 98.0.4758.80 config directory
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include chromium-allow-widevine.local
# Persistent global definitions
include globals.local

ignore noexec ${HOME}

noexec ${DOWNLOADS}

include whitelist-common-noexec.inc

# Misc. from chromium-common.profile
noexec ${HOME}/.config/chromium-flags.conf
noexec ${HOME}/.pki

# Chromium dirs
noexec ${HOME}/.config/chromium/AutofillRegex
noexec ${HOME}/.config/chromium/AutofillStates
noexec ${HOME}/.config/chromium/CertificateRevocation
noexec ${HOME}/.config/chromium/ClientSidePhishing
noexec ${HOME}/.config/chromium/Crash Reports
noexec ${HOME}/.config/chromium/Crowd Deny
noexec ${HOME}/.config/chromium/Default
noexec ${HOME}/.config/chromium/DesktopSharingHub
noexec ${HOME}/.config/chromium/Dictionaries
noexec ${HOME}/.config/chromium/FileTypePolicies
noexec ${HOME}/.config/chromium/FirstPartySetsPreloaded
noexec ${HOME}/.config/chromium/Floc
noexec ${HOME}/.config/chromium/GrShaderCache
noexec ${HOME}/.config/chromium/MEIPreload
noexec ${HOME}/.config/chromium/NativeMessagingHosts
noexec ${HOME}/.config/chromium/OnDeviceHeadSuggestModel
noexec ${HOME}/.config/chromium/OptimizationHints
noexec ${HOME}/.config/chromium/OriginTrials
noexec ${HOME}/.config/chromium/PKIMetadata
noexec ${HOME}/.config/chromium/SSLErrorAssistant
noexec ${HOME}/.config/chromium/Safe Browsing
noexec ${HOME}/.config/chromium/SafetyTips
noexec ${HOME}/.config/chromium/ShaderCache
noexec ${HOME}/.config/chromium/Subresource Filter
noexec ${HOME}/.config/chromium/TLSDeprecationConfig
# enable DRM: noexec ${HOME}/.config/chromium/WidevineCdm
noexec ${HOME}/.config/chromium/ZxcvbnData
noexec ${HOME}/.config/chromium/hyphen-data

# Chromium files
noexec ${HOME}/.config/chromium/*.pma
noexec ${HOME}/.config/chromium/First Run
noexec ${HOME}/.config/chromium/Last Version
noexec ${HOME}/.config/chromium/Local State
noexec ${HOME}/.config/chromium/Safe Browsing Cookies
noexec ${HOME}/.config/chromium/Safe Browsing Cookies-journal
noexec ${HOME}/.config/chromium/SingletonCookie
noexec ${HOME}/.config/chromium/SingletonLock
noexec ${HOME}/.config/chromium/SingletonSocket
noexec ${HOME}/.config/chromium/Variations

# Redirect
include chromium.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

