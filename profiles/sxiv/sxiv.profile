# Firejail profile for sxiv
# Description: imlib2 based image viewer
# ** This custom profile based on feh profile **
# Persistent local customizations
include sxiv.local
# Persistent global definitions
include globals.local

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc

caps.drop all
net none
no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none

private-bin sxiv
private-cache
private-dev
private-etc fonts,sxiv
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

