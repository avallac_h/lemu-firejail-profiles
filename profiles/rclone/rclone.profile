# Firejail profile for rclone
# Description: Syncs files with cloud storage
# ** This custom profile based on rclone profile **
# Persistent local customizations
include rclone.local
# Persistent global definitions
include globals.local

blacklist /tmp/.X11-unix

noblacklist ${HOME}/.config/rclone
noblacklist ${HOME}/RClone

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.config/rclone
whitelist ${HOME}/.config/rclone
whitelist ${HOME}/RClone

include whitelist-var-common.inc

caps.drop all
netfilter
no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix,inet,inet6
seccomp
shell none

private-bin go,lsof,rclone
private-dev
private-tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

