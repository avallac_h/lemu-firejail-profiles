# Firejail profile for Deponia
# Description: A graphic adventure video game by Daedalic Entertainment
# Based on: BaldursGate.profile
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include Deponia.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.local/share/Daedalic Entertainment/Deponia

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.local/share/Daedalic Entertainment/Deponia
whitelist ${HOME}/.local/share/Daedalic Entertainment/Deponia

include whitelist-common.inc

apparmor
caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
shell none
#tracelog

disable-mnt
private-bin Deponia,deponia,basename,bash,cat,chmod,cut,dirname,grep,head,id,readlink,sed,sh,tail,uname
private-cache
private-dev
private-etc alternatives,asound.conf,ca-certificates,crypto-policies,drirc,fonts,group,host.conf,hostname,hosts,ld.so.cache,ld.so.preload,localtime,machine-id,nsswitch.conf,passwd,pki,pulse,resolv.conf,ssl
#private-opt Deponia
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

