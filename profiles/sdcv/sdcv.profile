# Firejail profile for sdcv
# Description: 	Feature-rich dictionary lookup program
# This file is overwritten after every install/update
# Persistent local customizations
include sdcv.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.stardict
noblacklist ${HOME}/.lingvo
noblacklist ${HOME}/.sdcv_history
noblacklist ${HOME}/.sdcv_ordering
noblacklist ${DESKTOP}

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.stardict
whitelist ${HOME}/.stardict
whitelist ${HOME}/.lingvo

include whitelist-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
net none
nodbus
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix,netlink
seccomp
shell none

# private-bin sdcv
private-dev
private-tmp

noexec ${HOME}
noexec /tmp

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

