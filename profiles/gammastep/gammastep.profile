# Firejail profile for gammastep
# Description: Adjusts the color temperature of your screen according to your surroundings
# Based on: redshift.profile
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include gammastep.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/gammastep

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

mkdir ${HOME}/.config/gammastep
whitelist ${HOME}/.config/gammastep
include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
machine-id
netfilter
no3d
nodvd
nogroups
noinput
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix,inet,inet6
seccomp
shell none
tracelog

disable-mnt
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

