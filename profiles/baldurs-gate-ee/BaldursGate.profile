# Firejail profile for Baldur's Gate - Enhanced Edition
# Description: A remake of the 1998 RPG Baldur's Gate
# Based on: -
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include BaldursGate.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.local/share/Baldur*s Gate - Enhanced Edition

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

# FIXME: you need to create this directory by yourself:
#mkdir ${HOME}/.local/share/Baldur's Gate - Enhanced Edition
whitelist ${HOME}/.local/share/Baldur*s Gate - Enhanced Edition

include whitelist-common.inc

apparmor
caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
shell none
#tracelog

disable-mnt
private-bin BaldursGate,BaldursGate64,baldursgate,baldursgate64,basename,bash,cat,chmod,cut,dirname,grep,head,id,readlink,sed,tail,uname
private-cache
private-dev
private-etc alternatives,asound.conf,ca-certificates,crypto-policies,drirc,fonts,group,host.conf,hostname,hosts,ld.so.cache,ld.so.preload,localtime,machine-id,nsswitch.conf,passwd,pki,pulse,resolv.conf,ssl
#private-opt BaldursGate
private-tmp

dbus-user none
dbus-system none

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

