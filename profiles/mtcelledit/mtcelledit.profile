# Firejail custom profile for mtCellEdit based on leafpad profile
# Description: Mark Tyler's lightweight spreadsheet editor
# This file is NOT overwritten after every install/update
# Persistent local customizations
include featherpad.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.config/mtcelledit
noblacklist ${HOME}/.config/mtcelledit-qt4
noblacklist ${HOME}/.config/mtcelledit-qt5
noblacklist ${DOCUMENTS}

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc
include disable-xdg.inc

include whitelist-var-common.inc

apparmor
caps.drop all
net none
# no3d
nodbus
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

private-bin mtcelledit
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

# memory-deny-write-execute - breaks on Arch

join-or-start mtcelledit

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

