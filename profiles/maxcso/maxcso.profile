# Firejail profile for maxcso
# Description: File archiver with high compression ratio
# This file is overwritten after every install/update
#quiet
# Persistent local customizations
include maxcso.local
# Persistent global definitions
include globals.local

blacklist ${RUNUSER}/wayland-*

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc

apparmor
caps.drop all
hostname maxcso
ipc-namespace
machine-id
net none
no3d
nodvd
#nogroups
nonewprivs
#noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog
x11 none

private-bin maxcso
private-cache
private-dev

dbus-user none
dbus-system none

memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

