# Firejail profile alias for virtualbox
# Description: x86 virtualization solution
# This file is overwritten after every install/update

# Redirect
include virtualbox.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

