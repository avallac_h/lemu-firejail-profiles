# Firejail profile for aegisub
# Description: General-purpose subtitle editor with ASS/SSA support
# Based on: avidemux.profile
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include aegisub.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.aegisub

# Allow lua (blacklisted by disable-interpreters.inc)
include allow-lua.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc

mkdir ${HOME}/.aegisub
whitelist ${HOME}/.aegisub
whitelist /usr/share/aegisub
include whitelist-common.inc
include whitelist-player-common.inc
include whitelist-runuser-common.inc
include whitelist-usr-share-common.inc
include whitelist-var-common.inc 

apparmor
caps.drop all
net none
nodvd
nogroups
noinput
nonewprivs
noroot
notv
nou2f
novideo
protocol unix
seccomp
seccomp.block-secondary
shell none
tracelog

private-bin aegisub*,bash,lsb_release,lua*,sh,uname
private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

ignore memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

