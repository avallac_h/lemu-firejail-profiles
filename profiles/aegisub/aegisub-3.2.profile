# Firejail alias profile for aegisub-3.2
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include aegisub-3.2.local
# added by included profile
#include globals.local

# Redirect
include aegisub.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

