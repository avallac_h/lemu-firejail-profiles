# Firejail profile for flac
# Description: FLAC encoder/decoder
# Based on: mp3splt.profile
# This file SHOULD NOT BE OVERWRITTEN after every install/update
quiet
# Persistent local customizations
include flac.local
# Persistent global definitions
include globals.local

noblacklist ${MUSIC}

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc
include disable-X11.inc
include disable-xdg.inc

include whitelist-var-common.inc

apparmor
caps.drop all
ipc-namespace
machine-id
net none
no3d
nodvd
nogroups
noinput
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
seccomp.block-secondary
shell none
tracelog
x11 none

disable-mnt
private-bin flac,metaflac
private-cache
private-dev
private-etc alternatives,ld.so.cache,ld.so.preload

dbus-user none
dbus-system none

memory-deny-write-execute

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

