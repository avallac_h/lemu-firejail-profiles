# Firejail alias profile for metaflac
# This file SHOULD NOT BE OVERWRITTEN after every install/update
# Persistent local customizations
include metaflac.local
# added by included profile
#include globals.local

noblacklist ${PICTURES}

# Redirect
include flac.profile

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

