# Firejail profile for kicad
# Description: 
#
# Persistent local customizations
include kicad.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/kicad

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-xdg.inc

apparmor
caps.drop all
ipc-namespace
net none
nodvd
nogroups
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix
seccomp
shell none
tracelog

private-cache
private-dev
private-tmp

dbus-user none
dbus-system none

join-or-start kicad

# vim: set syntax=firejail ts=4 sts=4 sw=4 tw=0 et:

